
// Get the modal
var gabmodal = document.getElementById("gabrielModel");
var harmodal = document.getElementById("harrisonModel");
var rebmodal = document.getElementById("rebeccaModel");
var abimodal = document.getElementById("abigailModel");
var noamodal = document.getElementById("noahModel");
var aiymodal = document.getElementById("aiyshaModel");

// Get the button that opens the modal
var gabbtn = document.getElementById("gabrielBtn");
var harbtn = document.getElementById("harrisonBtn");
var rebbtn = document.getElementById("rebeccaBtn");
var abibtn = document.getElementById("abigailBtn");
var noabtn = document.getElementById("noahBtn");
var aiybtn = document.getElementById("aiyshaBtn");


// When the user clicks the button, open the modal
gabbtn.onclick = function() {
  gabmodal.style.display = "block";
}

harbtn.onclick = function() {
  harmodal.style.display = "block";
}

rebbtn.onclick = function() {
  rebmodal.style.display = "block";
}

abibtn.onclick = function() {
  abimodal.style.display = "block";
}

noabtn.onclick = function() {
  noamodal.style.display = "block";
}
aiybtn.onclick = function() {
  aiymodal.style.display = "block";
}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == gabmodal) {
    gabmodal.style.display = "none";
  }
  else if (event.target == harmodal) {
    harmodal.style.display = "none";
  }
  else if (event.target == rebmodal) {
    rebmodal.style.display = "none";
  }
  else if (event.target == abimodal) {
    abimodal.style.display = "none";
  }
  else if (event.target == noamodal) {
    noamodal.style.display = "none";
  }
  else if (event.target == aiymodal) {
    aiymodal.style.display = "none";
  }
}
